/****************************************************************************
 *              Declaration de mes variables                                *
 * *************************************************************************/

let short = ''
let long = ''
let id = ''
let typeT = ''
let idlignes = ''
let zoneArrets = ''
let nomArrets = ''
let numero = ''
let datatype = ''
let datacolor = ''
let textColor = ''
let modeT = ''
let choix = ''
let pseudo = ''

/***************************************************************************
 *                  $('document').ready(function (){}                      *
 *                  execute le code quand la page est chargée              *
 ***************************************************************************/
$('document').ready(function () {


    /*************************************************************************
     *            Swal est associé a la librairie sweet alert 2              *
     *            pour styliser l'alert js                                   *
     *************************************************************************/
    Swal.fire({
        title: 'Covid 19',
        text: 'Port du masque obligatoire sur toutes nos lignes',
        icon: 'warning',
        confirmButtonText: 'Comprit!'
    })

    /**************************************************************************
     *                  gestion des bouton au click pour                      *
     *              fermer la cession ou retourner a l'accueil appli          *
     *                                                                        *
     **************************************************************************/

    $('#outCession').click(function () {
        localStorage.clear()
        document.location.href = 'index.html'

    })

    $('#returnAccueil').click(function () {

        document.location.href = 'accueil.html'

    })


    /**************************************************************************
     *   recuperation dans le local storage de l'avatar et du pseudo          *
     **************************************************************************/

    choix = localStorage.getItem('avatar');
    pseudo = localStorage.getItem('pseudo');

    /**************************************************************************
     *   integration de l'avatar et du pseudo                                 *
     **************************************************************************/
    $('#pseudo').text('Bonjour ' + pseudo);
    $('#avatar').attr('src', choix);

    /**************************************************************************
     *  initialisation de la map leaflet selon la doc                         *
     **************************************************************************/

    let mymap = L.map('mapid').setView([45.1667, 5.7167], 10);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibWlja2FlbHYiLCJhIjoiY2tlZHp5ZHpnMGI2NzJ6b3lneXkyMmlnZSJ9.qw_xV6G2HNJliXo2eaTjKw'
    }).addTo(mymap);
    $('#mapid').hide();

    /**************************************************************************
     *   ajax(fetch) sur l API de la metro pour recuperer                     *
     *   toutes les lignes et leurs info en generales                         *
     **************************************************************************/
    $.ajax({


        url: 'http://data.metromobilite.fr/api/routers/default/index/routes',


        success: function (data) {


            res(data)
        },


        error: function () {

            console.log("Désolé, aucun résultat trouvé.");
        }

    });
    /**************************************************************************
     *   Fonction de lecture du json obtenu par l'ajax et recup des infos     *
     *    et integration dans la page                                          *
     **************************************************************************/
    function res(data) {

        for (let i = 0; i < data.length; i++) {
            short = data[i].shortName;
            long = data[i].longName;
            id = data[i].id;
            typeT = data[i].type;
            const color = data[i].color;
            const textColor = data[i].textColor;
            const mode = data[i].mode;

            $("#num").append('<p>' + numero + '</p>')
            $("#num").css("background-color", "#" + datacolor);

            let li = "<li id=" + id + " data-type=" + typeT + " data-short=" + short + " data-textColor= " + textColor + " data-mode=" + mode + " data-color=" + color + "><p style='color: white ;background-color: #" + color + "'>" + short + "</p>" + long + "</li>";
            $('#touteLignes').append(li)

        }

        /**************************************************************************
         *   ouverture  de la div avec integration des infos                       *
         **************************************************************************/
        $("li").click(function (event) {
            event.preventDefault()
            idlignes = $(this).attr('id');
            datatype = $(this).attr('data-type');
            datacolor = $(this).attr('data-color');
            textColor = $(this).attr('data-textColor');
            numero = $(this).attr('data-short');
            modeT = $(this).attr('data-mode');





            $("#details").attr("class", "fullscreen");
            $("#details").show();
            $('#mapid').show()
            const offset = $(this).offset();
            $("#details").css('top', offset.top);


            $("#num").append('<p>' + numero + '</p>')
            $("#num").css("background-color", "#" + datacolor);

            if (modeT == "TRAM") {
                let typeTram = '<img src=./assets/tram.png>';
                $("#type").append(typeTram)
            }
            else if (modeT == "RAIL") {
                let typeTrain = '<img src=./assets/train.png>';
                $("#type").append(typeTrain)
            }
            else {
                let typeBus = '<img src=./assets/bus.png>';
                $("#type").append(typeBus)
            }

            $("#service").append('<p>' + "Cette ligne depend du réseau :" + datatype + '</p>')

            /**************************************************************************
             *   ajax(fetch)pour obtenir la liste des arrets d'une lignes             *
             **************************************************************************/
            let url = `http://data.metromobilite.fr/api/ficheHoraires/json?route=` + idlignes;

            $.ajax({
                url: url,

                success: function (dataligne) {


                    listeArrets(dataligne);
                    coordonnees(dataligne);
                    polyline();



                },

                error: function () {

                    console.log("Désolé, aucun résultat trouvé.");
                },

            })




        });
    }
    /**************************************************************************
     *   ajout d'une ligne aux favoris et localstorage des nfos               *
     **************************************************************************/
    $("#fav").click(function (event) {

        localStorage.setItem('ID', idlignes);


        let objJson = {
            numeroBus: numero,
            colorLigne: datacolor,
            type: datatype,
            mode: modeT
        }



        let objLinea = JSON.stringify(objJson);

        localStorage.setItem("obj", objLinea);

        document.location.href = 'favoris.html';

    })

    /**************************************************************************
     *   Vidage de la div au clik bouton fermer la div                        *
     **************************************************************************/
    $("#bouton").click(function (event) {
        $("#details").removeClass("fullscreen");
        $("#num").empty();
        $("#type").empty();
        $("#service").empty();
        $("#btnArretsBox").empty();
        $("#ulListingArrets").empty();

        $("#details").hide();



    });


    /**************************************************************************
     *   fonction pour lire les arrets et les integrer...                     *
     **************************************************************************/

    function listeArrets(dataligne) {
        if (dataligne[0].arrets.length !== 0) {

            for (let i = 0; i < dataligne[0].arrets.length; i++) {
                nomArrets = dataligne[0].arrets[i].stopName;
                nomLat = dataligne[0].arrets[i].lat;




                let li = "<li>" + nomArrets + "</li>";
                $('#ulListingArrets').append(li)
            }
        } else {
            let ina = "<p>Cette ligne est inactive actuellement</p>";
            $('#ulListingArrets').append(ina)
        }


    }
    /**************************************************************************
     *   fonction pour lire les coordonnées des arrets et les integrer...     *
     **************************************************************************/
    function coordonnees(dataligne) {

        for (let i = 0; i < dataligne[0].arrets.length; i++) {
            L.marker([dataligne[0].arrets[i].lat,
            dataligne[0].arrets[i].lon]).bindPopup(dataligne[0].arrets[i].stopName).addTo(mymap);


        }
    }

    /**************************************************************************
     *fonction pour lire les coordonnées des tracer de ligne et les integrer...*
     **************************************************************************/
    function polyline() {
        let url2 = `http://data.metromobilite.fr/api/lines/json?types=ligne&codes=` + idlignes;
        $.ajax({
            url: url2,

            success: function (polyline) {

                L.geoJSON(polyline).addTo(mymap);

            },


            error: function () {

                console.log("Désolé, aucun résultat trouvé.");
            }
        })

    }




});
