




function addDarkmodeWidget() {
    const options = {
        bottom: '64px', // default: '32px'
        right: 'unset', // default: '32px'
        left: '90%', // default: 'unset'
        time: '0.5s', // default: '0.3s'
        mixColor: '#010587', // default: '#fff'
        backgroundColor: '#4C0A20',  // default: '#fff'
        buttonColorDark: '#010587',  // default: '#100f2c'
        buttonColorLight: '#4C0A20', // default: '#fff'
        saveInCookies: false, // default: true,
        label: '🌓', // default: ''
        autoMatchOsTheme: true // default: true
    }

    const darkmode = new Darkmode(options);
    darkmode.showWidget();
}
window.addEventListener('load', addDarkmodeWidget);
