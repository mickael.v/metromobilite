$('document').ready(function () {

    /**************************************************************************
     *  chargement dans le localstorage des pseudo et avatar                  *
     **************************************************************************/
    function avatar() {
        $('#submit').click(function () {
            console.log('fontion ok')
            let avatar = $('input[name=avatar]:checked').val();
            let pseudo = $('#fname').val();

            localStorage.setItem('avatar', avatar);
            localStorage.setItem('pseudo', pseudo);

            document.location.href = 'accueil.html';
        })
    }

    avatar();

});