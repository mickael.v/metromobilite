let idlignes = ''
let choix = ''
let pseudo = ''
let timestamp = ''

$('document').ready(function () {


    $('#outCession').click(function () {
        localStorage.clear()
        document.location.href = 'index.html'
    })

    $('#returnAccueil').click(function () {
        document.location.href = 'accueil.html'
    })

    choix = localStorage.getItem('avatar');
    pseudo = localStorage.getItem('pseudo');


    $('#pseudo').text('Bonjour ' + pseudo);
    $('#avatar').attr('src', choix);

    $("#details").hide();

    $.ajax({
        url: 'http://data.metromobilite.fr/api/routers/default/index/routes',

        success: function (data) {
            res(data)
        },

        error: function () {
            console.log("Désolé, aucun résultat trouvé.");
        }

    });
    function res(data) {

        for (let i = 0; i < data.length; i++) {
            let short = data[i].shortName;
            //  let long = data[i].longName;
            let id = data[i].id;
            //  let typeT= data[i].type;
            let color = data[i].color;
            let textColor = data[i].textColor;
            let mode = data[i].mode;

            let p = "<p style='background-color: #" + color + "' id=" + id + " data-short=" + short + " data-textColor= " + textColor + " data-mode=" + mode + " data-color=" + color + ">" + short + "</p>";
            $('#liste').append(p)

        }

        $("p").one("click", function () {
            $("#details").attr("class", "fullscreen");
            $("#details").show();
            const offset = $(this).offset();
            $("#details").css('top', offset.top);
            idlignes = $(this).attr('id');
            horaires();

        })

    }

    $("#bouton").click(function (event) {
        $("#details").removeClass("fullscreen");
        $("#nextHours").empty();
        $("#nextHoursInverse").empty();
        $("#details").hide();
    });

    function horaires() {
        let timestamp = Math.round(new Date().getTime());
        console.log(timestamp)
        let url = `http://data.metromobilite.fr/api/ficheHoraires/json?route=` + idlignes + `&time=` + timestamp;
        $.ajax({
            url: url,
            success: function (data1) {

                console.log(data1);
                console.log(url);
                afficherHoraires(data1);
                afficherHorairesInverse(data1);
            },

            error: function () {
                console.log("Désolé, aucun résultat trouvé.");
            }
        })
    }
    function afficherHoraires(data1) {
        console.log(data1)

        for (let j = 0; j < data1[0].arrets.length; j++) {
            let nomArrets = data1[0].arrets[j].stopName;


            let next = data1[0].arrets[j].trips[0];

            console.log(secondsToHms(next))
            let li = "<li>" + nomArrets + "Prochain passage : " + secondsToHms(next) + "</li>";
            $('#nextHours').append(li)

        }
    }

    function afficherHorairesInverse(data1) {
        console.log(data1)

        for (let j = 0; j < data1[1].arrets.length; j++) {
            let nomArretsInverse = data1[1].arrets[j].stopName;
            let nextInverse = data1[1].arrets[j].trips[0];
            let li = "<li>" + nomArretsInverse + "Prochain passage : " + secondsToHmsInverse(nextInverse) + "</li>";
            $('#nextHoursInverse').append(li)

        }
    }
});



function secondsToHms(d) {
    d = Number(d);

    var h = Math.floor(d / 3600) + 2;
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
}
function secondsToHmsInverse(d) {
    d = Number(d);

    var h = Math.floor(d / 3600) + 2;
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
}

