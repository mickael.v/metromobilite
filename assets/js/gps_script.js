
let lat = '';
let long = '';
let choix = '';
let pseudo = '';

$('document').ready(function () {

    $('#outCession').click(function () {
        localStorage.clear()
        document.location.href = 'index.html'
    })

    $('#returnAccueil').click(function () {
 document.location.href = 'accueil.html'

    })

    choix = localStorage.getItem('avatar');
    pseudo = localStorage.getItem('pseudo');
    console.log(choix)
    console.log(pseudo)

    $('#pseudo').text('Bonjour ' + pseudo);
    $('#avatar').attr('src', choix);

    choix = localStorage.getItem('avatar');

    let mymap = ''
    navigator.geolocation.getCurrentPosition(successPosition, errorPosition);

    function successPosition(pos) {
        if (pos) {

            lat = pos.coords.latitude;
            long = pos.coords.longitude;
            console.log(lat);
            console.log(long);

            mymap = L.map('mapid').setView([lat, long], 17);
            var greenIcon = L.icon({
                iconUrl: choix,

                iconSize: [38, 95], // size of the icon
                shadowSize: [50, 64], // size of the shadow
                iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
                shadowAnchor: [4, 62],  // the same for the shadow
                popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
            });
            let marker = L.marker([lat, long], { icon: greenIcon }).bindPopup("<b>Votre position").openPopup().addTo(mymap);;


            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1,
                accessToken: 'pk.eyJ1IjoibWlja2FlbHYiLCJhIjoiY2tlZHp5ZHpnMGI2NzJ6b3lneXkyMmlnZSJ9.qw_xV6G2HNJliXo2eaTjKw'
            }).addTo(mymap);

            fetchGps();


        }
    }

    function errorPosition() {
        alert('No position');
    }



    function fetchGps() {

        let url = `https://data.metromobilite.fr/api/linesNear/json?x=` + long + `&y=` + lat + `&dist=400&details=true`;
        console.log(url);


        $.ajax({



            url: url,

            success: function (gps) {

                console.log(gps)

                arretsProche(gps);

            },

            error: function () {
                console.log("Désolé, aucun résultat trouvé.");
            },


        })
    }

    function arretsProche(gps) {
        for (let i = 0; i < gps.length; i++) {
            L.marker([gps[i].lat, gps[i].lon]).bindPopup(gps[i].name + gps[i].lines).addTo(mymap);
        }
    }



});